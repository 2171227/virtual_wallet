require('./bootstrap');

import Vue from 'vue';
import axios from 'axios';
import Login from './components/authentication/LoginPage.vue';
import ChangePassword from './components/users/ChangePasswordPage.vue';
import VueRouter from 'vue-router';
Vue.use(VueRouter);


// # Routes
const routes = [
    { path: '/', redirect: '/login' },
    { path: '/login', component: Login, name: 'login'},
    { path: '/change/password', component: ChangePassword, name: 'change-password', meta: { requiresAuth: true }},
];


const router = new VueRouter({
    routes:routes
})

// # Application
new Vue({
    el: '#app',
    router,
});